<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 18.06.2016.
 * Time: 16:55
 */
?>

@extends('welcome')

@section('body')
   <section id="intro-section">
        @include('pages.sections.intro')
   </section>
   <section id="about-section" style="padding-bottom: 70px;">
       @include('pages.sections.about')
   </section>
   <section id="example-section">
       @include('pages.sections.examples')
   </section>
   <section id="team-section">
       @include('pages.sections.team')
   </section>
   <section id="contact-section">
       @include('pages.sections.contact')
   </section>
@endsection

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function(){
            $(window).scroll(function () {

                var about = $("#about-section").offset().top;
                var _top = $(window).scrollTop();

                if((_top+300) >= about) {

                    $(".about-box-frame").animate({
                        left: "0px"
                    },1000, "linear");
                /*    $(".about-box-frame").fadeIn("slow");*/
                }
            });
        });


        function openTeamModal(membernr) {

            var name, description;

             switch (membernr){
                 case 1:
                     name = "Kaspars Vanags";
                     description = "<p class='text-center' style='font-family: Lato, Arial; font-weight: 600; color: #212121;'>" +
                             "<span class='accent-text' style='font-size: 0.7em;'><i class='fa fa-quote-left' aria-hidden='true'></i></span>" +
                             "I'm little bit obsessive when talk about \"New Ideas\" and possibility to learn something new." +
                             " This project \"Kaito D.H | Media\" is result of my obsession and huge urge to learn something refreshing." +
                             " With small team we have stepped first steps towards new achievements." +
                             " <span class='accent-text' style='font-size: 0.7em;'><i class='fa fa-quote-right' aria-hidden='true'></i></span></p>" +
                             "<hr><p><span class='accent-text'><i class='fa fa-envelope-o' aria-hidden='true'></i></span> kvanags@niknais.com </p>" +
                             "<p><span class='accent-text'><i class='fa fa-twitter' aria-hidden='true'></i></span> Ponchiks_Labais</p>";
                     break;
                 case 2:
                     name = "Matt Howlett";
                     description = "<p class='text-center' style='font-family: Lato, Arial; font-weight: 600; color: #212121;'>" +
                             "<span class='accent-text' style='font-size: 0.7em;'><i class='fa fa-quote-left' aria-hidden='true'></i></span>" +
                             "It's not easy to create something new or old but in new light, but we are here to give our best result without loosing our or clients identity." +
                             " <span class='accent-text' style='font-size: 0.7em;'><i class='fa fa-quote-right' aria-hidden='true'></i></span></p>" +
                             "<hr><p><span class='accent-text'><i class='fa fa-envelope-o' aria-hidden='true'></i></span> mhowlett@niknais.com </p>" +
                             "<p><span class='accent-text'><i class='fa fa-twitter' aria-hidden='true'></i></span> MattDHowlett</p>";
                     break;
                 case 3:
                     name = "Edgars Každaļevičs";
                     description = "<p class='text-center' style='font-family: Lato, Arial; font-weight: 600; color: #212121;'>" +
                             "<span class='accent-text' style='font-size: 0.7em;'><i class='fa fa-quote-left' aria-hidden='true'></i></span>" +
                             "There is nothing we can't archive. I like to know more and deeper about principles we use in our daily life's" +
                             "to explore them and use in my advantage. Ability to dig deeper means you can achieve more." +
                             " <span class='accent-text' style='font-size: 0.7em;'><i class='fa fa-quote-right' aria-hidden='true'></i></span></p>" +
                            "<hr><p><span class='accent-text'><i class='fa fa-envelope-o' aria-hidden='true'></i></span> ekazdalevics@niknais.com </p>";
                     break;

             }

            $("#team-member-name").html(name);
            var body = $("#team-modal").find(".modal-body");
            body.html("");
            body.append(description);
            $("#team-modal").modal("show");
        }

    </script>
@endsection
