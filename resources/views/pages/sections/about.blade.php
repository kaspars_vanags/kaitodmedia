<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.03.2017.
 * Time: 19:26
 */

        ?>


<div class="row">
    <div class="col-md-4 col-md-offset-4" id="about-us-title" style="margin-top: 50px;">
        <h1 class="text-center">WE DO</h1>
        <hr class="custom-hr">
        <p class="text-center" style="font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">Maybe there is something we can do for you</p>
    </div>
</div>
<div class="row" style="margin-top: 60px;">
    <div class="col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-10 col-xs-offset-1 col-sm-offset-0 col-md-offset-0">
                <div class="about-box-frame">
                    <div class="row">
                        <div class="col-sm-3 col-xs-12">
                            <p class="text-center about-box-icon">
                                <i class="fa fa-desktop" aria-hidden="true"></i>
                            </p>
                        </div>
                        <div class="col-sm-9  col-xs-12">
                            <p class="about-box-title">
                                Web Development
                            </p>
                            <hr style="border-color: #212121; margin-top: 10px; margin-bottom: 10px;">
                            <p style="font-size: 0.9em; color: #cacaca;">
                                From single idea to fully working web page.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-10 col-xs-offset-1 col-sm-offset-0 col-md-offset-0">
                <div class="about-box-frame">
                    <div class="row">
                        <div class="col-sm-3 col-xs-12">
                            <p class="text-center about-box-icon">
                                <i class="fa fa-mobile" aria-hidden="true"></i>
                            </p>
                        </div>
                        <div class="col-sm-9 col-xs-12">
                            <p class="about-box-title">
                                Mobile Development
                            </p>
                            <hr style="border-color: #212121; margin-top: 10px; margin-bottom: 10px;">
                            <p style="font-size: 0.9em; color: #cacaca;">
                                Currently we work only with Android but we are looking forward to iOS.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-10 col-xs-offset-1 col-sm-offset-0 col-md-offset-0">
                <div class="about-box-frame">
                    <div class="row">
                        <div class="col-sm-3 col-xs-12">
                            <p class="text-center about-box-icon">
                                <i class="fa fa-wrench" aria-hidden="true"></i>
                            </p>
                        </div>
                        <div class="col-sm-9 col-xs-12">
                            <p class="about-box-title">
                                Fix
                            </p>
                            <hr style="border-color: #212121; margin-top: 10px; margin-bottom: 10px;">
                            <p style="font-size: 0.9em; color: #cacaca;">
                                We can fix or improve existing projects.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
{{--        </div>
        <div class="row" style="margin-top: 40px;">--}}
{{--             
            <div class="col-md-4 col-sm-6 col-xs-10 col-xs-offset-1 col-sm-offset-0 col-md-offset-0" >
                <div class="about-box-frame">
                    <div class="row">
                        <div class="col-sm-3 col-xs-12">
                            <p class="text-center about-box-icon">
                                <i class="fa fa-universal-access" aria-hidden="true"></i>
                            </p>
                        </div>
                        <div class="col-sm-9 col-xs-12">
                            <p class="about-box-title">
                                Branding
                            </p>
                            <hr style="border-color: #212121; margin-top: 10px; margin-bottom: 10px;">
                            <p style="font-size: 0.9em; color: #cacaca;">
                                Creating business and personal branding
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-10 col-xs-offset-1 col-sm-offset-0 col-md-offset-0">
                <div class="about-box-frame">
                    <div class="row">
                        <div class="col-sm-3 col-xs-12">
                            <p class="text-center about-box-icon">
                                <i class="fa fa-camera-retro" aria-hidden="true"></i>
                            </p>
                        </div>
                        <div class="col-sm-9 col-xs-12">
                            <p class="about-box-title">
                                Video/Photo
                            </p>
                            <hr style="border-color: #212121; margin-top: 10px; margin-bottom: 10px;">
                            <p style="font-size: 0.9em; color: #cacaca;">
                                Video and Photo editing. From music videos to simple photo session.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-10 col-xs-offset-1 col-sm-offset-0 col-md-offset-0">
                <div class="about-box-frame">
                    <div class="row">
                        <div class="col-sm-3 col-xs-12">
                            <p class="text-center about-box-icon">
                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                            </p>
                        </div>
                        <div class="col-sm-9 col-xs-12">
                            <p class="about-box-title">
                                Plan out
                            </p>
                            <hr style="border-color: #212121; margin-top: 10px; margin-bottom: 10px;">
                            <p style="font-size: 0.9em; color: #cacaca;">
                                Have idea but don't know how to achieve that. We could help you with that.
                            </p>
                        </div>
                    </div>
                </div>
            </div> --}}
{{--            <div class="col-md-4">
                <div class="about-box-frame">
                    <div class="row">
                        <div class="col-md-3">
                            <p class="text-center about-box-icon">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </p>
                        </div>
                        <div class="col-md-9">
                            <p class="about-box-title">
                                Love service
                            </p>
                            <hr style="border-color: #212121; margin-top: 10px; margin-bottom: 10px;">
                            <p style="font-size: 0.9em; color: #cacaca;">
                                We do our best to help people meet their significant-others.
                            </p>
                        </div>
                    </div>
                </div>
            </div>--}}
{{--        </div>--}}
    </div>
</div>
