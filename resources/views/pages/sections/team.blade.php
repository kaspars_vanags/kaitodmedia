<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.03.2017.
 * Time: 19:25
 */

        ?>



<div class="row">
    <div class="col-md-4 col-md-offset-4" style="margin-top: 50px;">
        <h1 class="text-center title">TEAM</h1>
        <hr class="custom-hr">
        <p class="text-center" style="font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">Introducing our small team</p>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <!-- NEW concept -->
        <div class="row" style="margin-top: 40px;">
            <div class="col-xs-12 col-sm-4">
                <div class="row" style="margin-top: 40px;">
                    <div class="col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="hexagon" style="background-image: url('../img/team/DSC_1273.jpg')">
                            <div class="hexTop"></div>
                            <div class="hexBottom"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="team-text-box">
                            <h3 class="text-center" style="font-family: Roboto, 'Arial'; font-weight: 200; color: #fff;">Kaspars Vanags</h3>
                            <p class="text-center accent-text" style="font-family: 'Lato', 'Arial'; font-weight: 600;">Founder, Web developer</p>
                        </div>
                        <p class="text-center" style="font-size: 1.5em;">
                            <button class="btn btn-raised team-button" onclick="openTeamModal(1)">
                                MORE <span style="padding-left: 10px; font-size: 1.2em;"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                            </button>
                            {{--                    <a href="/face" style="color: #fff;"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                <a href="/google" style="color: #fff;"><i class="fa fa-google-plus-official" aria-hidden="true"></i></a>
                                                <a href="/twitter" style="color: #fff;"><i class="fa fa-twitter" aria-hidden="true"></i></a>--}}
                        </p>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="row" style="margin-top: 40px;">
                    <div class="col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="hexagon" style="background-image: url('../img/img/image_not_found.png')">
                            <div class="hexTop"></div>
                            <div class="hexBottom"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="team-text-box">
                            <h3 class="text-center" style="font-family: Roboto, 'Arial'; font-weight: 200; color: #fff;">Matt Howlett</h3>
                            <p class="text-center accent-text" style="font-family: 'Lato', 'Arial'; font-weight: 600;">Creative director</p>
                        </div>
                        <p class="text-center" style="font-size: 1.5em;">
                            <button class="btn btn-raised team-button" onclick="openTeamModal(2)">
                                MORE <span style="padding-left: 10px; font-size: 1.2em;"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                            </button>
                            {{--                    <a href="/face" style="color: #fff;"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                <a href="/google" style="color: #fff;"><i class="fa fa-google-plus-official" aria-hidden="true"></i></a>
                                                <a href="/twitter" style="color: #fff;"><i class="fa fa-twitter" aria-hidden="true"></i></a>--}}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="row" style="margin-top: 40px;">
                    <div class="col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="hexagon" style="background-image: url('../img/team/edgars_1.jpg')">
                            <div class="hexTop"></div>
                            <div class="hexBottom"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="team-text-box">
                            <h3 class="text-center" style="font-family: Roboto,'Arial'; font-weight: 200; color: #fff;">Edgars Každaļevičs</h3>
                            <p class="text-center accent-text" style="font-family: 'Lato', 'Arial'; font-weight: 600;">Web Developer</p>
                        </div>
                        <p class="text-center" style="font-size: 1.5em;">
                            <button class="btn btn-raised team-button" onclick="openTeamModal(3)">
                                MORE <span style="padding-left: 10px; font-size: 1.2em;"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                            </button>
                            {{--                    <a href="/face" style="color: #fff;"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                <a href="/google" style="color: #fff;"><i class="fa fa-google-plus-official" aria-hidden="true"></i></a>
                                                <a href="/twitter" style="color: #fff;"><i class="fa fa-twitter" aria-hidden="true"></i></a>--}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="team-modal">
    <div class="modal-dialog">
        <div class="modal-content" style="border-radius: 0px;">
            <div class="modal-header">
                <p class="text-center modal-close close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></p>
                <h3 class="text-center" id="team-member-name">Edgars Každaļevičs</h3>
            </div>
            <div class="modal-body">
                <p class="text-justify" id="team-member-description">

                </p>
            </div>
        </div>
    </div>
</div>