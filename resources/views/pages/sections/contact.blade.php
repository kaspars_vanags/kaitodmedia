<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.03.2017.
 * Time: 19:25
 */

        ?>


<div class="row">
    <div class="col-md-4 col-md-offset-4" style="margin-top: 70px;">
        <h1 class="text-center title">CONTACT US</h1>
        <hr class="custom-hr">
        <p class="text-center" style="font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">Still have questions? We've got answers.</p>
    </div>
</div>
@if (session()->has('flash_notification.message'))
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="alert alert-message-{{ session('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {!! session('flash_notification.message') !!}
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="row" style="margin-top: 40px; margin-bottom: 70px;">
            <form id="contact-form" method="POST" action="/mail/send">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-2">
                            <input type="text" class="form-control" name="name" placeholder="Name" required>
                        </div>
                        <div class="col-md-4">
                            <input type="email" class="form-control" name="email" placeholder="Email" required>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-top: 20px;">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <textarea class="form-control" rows="10" cols="5" style="height: 300px;" name="body" maxlength="400" minlength="10" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-raised send-message-button">SEND ENQUIRE</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

