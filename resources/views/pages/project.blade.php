<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 20.06.2016.
 * Time: 18:44
 */

?>

@extends('welcome')

@section('body')
{{--    <script src="{{ URL::asset('/js/sketch.js')}}"></script>--}}
{{--    <div id="nogl" style="display: none;">
        <h1>Aww, No WebGL :(</h1>
        <p>This experiment requires WebGL to run. Please enable it, or come back and visit with a <a href="http://caniuse.com/webgl" target="_blank">compatible browser</a>.</p>
    </div>--}}
    <section id="portfolio-index-section">
{{--        <div class="row">
            <div id="portfolio-index-section-2" style="margin-bottom: -5px;">

            </div>
        </div>--}}
        <div class="hidden-xs" id="portfolio-title" style="position: absolute; top: 250px; left: 200px;">
            <div class="col-sm-5 col-sm-offset-2">
                <h4 class="text-left"><span class="title-second" style="font-size: 1.5em; background: #337ab7; padding: 5px;"> PORTFOLIO</span></h4>
                <p class="text-left" style="font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">
                        This project is something we created in early 2017.
                </p>
                <p class="text-left" style="font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">
                        So maybe at current the moment we don't have a lot of projects
                        which we are available show to you, but please be our guest to see few of them.
                </p>
            </div>
        </div>
        <div class="row visible-xs">
            <div class="col-sm-5 col-sm-offset-2">
                <h4 class="text-left"><span class="title-second" style="font-size: 1.5em; background: #337ab7; padding: 5px;"> PORTFOLIO</span></h4>
                <p class="text-left" style="font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">
                    This project is something we created in early 2017.
                </p>
                <p class="text-left" style="font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">
                    So maybe at current the moment we don't have a lot of projects
                    which we are available show to you, but please be our guest to see few of them.
                </p>
            </div>
        </div>

{{--        <div class="row" style="height: 1000px;">
            <div class="col-md-5 col-md-offset-2" style="margin-top: 300px;">
                <h4 class="text-left"><span class="title-second" style="font-size: 1.5em"> PORTFOLIO</span></h4>
                <p class="text-left" style="font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">
                    This project is something we created in early 2017.
                </p>
                <p class="text-left" style="font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">
                    So maybe at current the moment we don't have a lot of projects
                    which we are available show to you, but please be our guest to see few of them.
                </p>
            </div>
        </div>--}}
    </section>
    <section id="portfolio-web-section">
        <div class="row">
            <div class="col-md-8">
                <div id="project-body" class="col-md-12">
                    <div class="row" style="margin-top: 30px;">
{{--                        <div class="col-md-4">
                            <img class="center-block" src="img/project/thirdnewtonslaw.png" height="300">
                            <h2 class="text-center">THIRD NEWTON'S LAW</h2>
                        </div>--}}
                        <div class="col-md-4 project-item">
                            <img class="center-block" src="img/project/niknaiscom.png" height="300">
                            <p class="text-center">
                                <a class="btn btn-raised project-example-button" href="http://www.niknais.com" target="_blank">
                                    VISIT <span style="padding-left: 10px; font-size: 1.2em;"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                                </a>
                            </p>
{{--                            <h2 class="text-center">NIKNAIS.COM</h2>--}}
                        </div>
                        <div class="col-md-4 project-item">
                            <img class="center-block" src="img/project/kdhawk.png" height="300">
                            <p class="text-center">
                                <a class="btn btn-raised project-example-button" href="http://kaitodmedia.com/" target="_blank">
                                    VISIT <span style="padding-left: 10px; font-size: 1.2em;"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                                </a>
                            </p>
                          {{--  <h2 class="text-center">K.D.HAWK</h2>--}}
                        </div>
                        <div class="col-md-4 project-item">
                            <img class="center-block" src="img/project/tripsavage.png" height="300">
                            <p class="text-center">
                                <a class="btn btn-raised project-example-button" href="http://tripsavage.com/" target="_blank">
                                    VISIT <span style="padding-left: 10px; font-size: 1.2em;"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 30px;">
                        <div class="col-xs-4 project-item">
                            <img class="center-block" src="img/project/rebuild_progress.png" height="300">
                         {{--   <h2 class="text-center">REBUILD.LV</h2>--}}
                        </div>
                        <div class="col-xs-4 project-item">
                            <img class="center-block" src="img/project/svini_progress.png" height="300">
                       {{--     <h2 class="text-center">SVINI.LV</h2>--}}
                        </div>
                        <div class="col-xs-4 project-item">
                            <img class="center-block" src="img/project/smaidi_progress.png" height="300">
                            {{--  <h2 class="text-center">SMAIDI.LV</h2>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 visible-xs visible-sm" style="background-color: #00B0FF; padding-top: 50px; padding-bottom: 50px;">
                    <h1 class="text-left" style="color: #212121; font-weight: 300;">WEB PROJECTS</h1>
                    {{--<hr style="border-color: #212121; margin-top: 10px; margin-bottom: 10px;">--}}
                    <p class="text-justify" style="font-family: 'Lato', 'Arial'; font-weight: 900; color: #212121;">
                        We are open for new ideas and new type of projects. Sometimes we do our stuff absolutely for free
                        because there is some ideas which we just want that they would come to life.
                    </p>
            </div>
            <div class="col-md-4 hidden-xs hidden-sm" style="background-color: #00B0FF; height: 800px;">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2" style="margin-top: 50%;">
                        <h1 class="text-left title" style="color: #212121;">WEB PROJECTS</h1>
                        {{--<hr style="border-color: #212121; margin-top: 10px; margin-bottom: 10px;">--}}
                        <p class="text-justify" style="font-family: 'Lato', 'Arial'; font-weight: 600; color: #212121;">
                            We are open for new ideas and new type of projects. Sometimes we do our stuff absolutely for free
                            because there is some ideas which we just want that they would come to life.
                        </p>
{{--                        <a href="/portfolio" class="btn btn-raised example-button">
                            VIEW EXAMPLES <span style="padding-left: 10px; font-size: 1.2em;"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                        </a>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="portfolio-web-tools-section">
        <div class="row" style="margin-top: 75px;">
            <div class="col-md-10 col-md-offset-1">
                <h1 class="text-center title" style="color: #00B0FF;">WEB TECHNOLOGIES</h1>
                <hr class="custom-hr">
                <p class="text-center" style="font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">List of few tools we use in daily bases.</p>
                <div class="row" id="project-web-tools-animate_1" style="margin-top: 70px;">
                    <div class="col-md-4">
                        <div class="row">
                            <img src="/img/img/php.png" class="center-block" width="100">
                        </div>
                        <div class="row" style="margin-top: 30px; font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">
                            <div class="col-xs-6 col-xs-offset-3 col-md-10 col-md-offset-1">
                                <p class="text-justify">
                                    As our base language for all new Web projects we use PHP. It is easy to use, one of
                                    most common web development languages and goes really well with OOP principles.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <img src="/img/img/sass.png" class="center-block" width="100">
                        </div>
                        <div class="row" style="margin-top: 30px; font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">
                            <div class="col-xs-6 col-xs-offset-3 col-md-10 col-md-offset-1">
                                <p class="text-justify">
                                    Of course we use CSS, but to step-up our visual game we are trying to implement as
                                    much as possible SASS. You can say we are using CSS on drugs for best visual performance.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <img src="/img/img/vuejs.png" class="center-block" width="100">
                        </div>
                        <div class="row" style="margin-top: 30px; font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">
                            <div class="col-xs-6 col-xs-offset-3 col-md-10 col-md-offset-1">
                                <p class="text-justify">
                                    For best user experience we are using VUE.JS as our main JavaScript framework. It helps us to improve code
                                    reusability as well as it makes easier to maintain clean code in front and back-end. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="project-web-tools-animate_2" style="margin-top: 70px; margin-bottom: 150px;">
                    <div class="col-md-4">
                        <div class="row">
                            <img src="/img/img/third.png" class="center-block" width="100">
                        </div>
                        <div class="row" style="margin-top: 30px; font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">
                            <div class="col-xs-6 col-xs-offset-3 col-md-10 col-md-offset-1">
                                <p class="text-justify">
                                    Drupal, WordPress or other CMS system. C#, Ruby or other common web development language.
                                    We are ready to help fix your existing project or give all necessary information
                                    that you maybe could consider create new project if it's necessary.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <img src="/img/img/laravel.png" class="center-block" width="100">
                        </div>
                        <div class="row" style="margin-top: 30px; font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">
                            <div class="col-xs-6 col-xs-offset-3 col-md-10 col-md-offset-1">
                                <p class="text-justify">
                                    For easier development and maintaining new projects, we are using "Laravel" framework. It
                                    allows as to save time/money/hassle. If you have existing project in different framework but
                                    you want migrate to "Laravel", we can help you.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <img src="/img/img/twitter.png" class="center-block" width="100">
                        </div>
                        <div class="row" style="margin-top: 30px; font-family: 'Lato', 'Arial'; font-weight: 600; color: #fff;">
                            <div class="col-xs-6 col-xs-offset-3 col-md-10 col-md-offset-1">
                                <p class="text-justify">
                                    Twitter/Facebook/Google or other soc.network with modern API is something we can apply
                                    for new or old projects. Extend your current project to be fully compatible with social network.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="portfolio-media-section">

    </section>

@endsection

@section("scripts")
{{--
    <script src="{{ URL::asset('js/sketch.js') }}"></script>
    <script src="{{ URL::asset('js/particle.js') }}" type="text/javascript"></script>
--}}

    <script>
        $(document).ready(function(){
            $(window).scroll(function () {

                var about = $("#portfolio-web-tools-section").offset().top;
                console.log(about);
                var _top = $(window).scrollTop();

                var h = $("#portfolio-web-tools-section").height();

                if((_top+200) >= about) {

                    $("#project-web-tools-animate_1").animate({
                        left: "0px"//(h-650)+"px",
                   /*     position: "relative"*/
                    },4000, "linear", function(){
                        $("#project-web-tools-animate_1").css("position", "initial");
                        moveSecondList();
                    });
                }
            });

            function moveSecondList(){
                var h = $("#portfolio-web-tools-section").height();

                $("#project-web-tools-animate_2").animate({
                    left: "0px"//(h-400)+"px"
                },1000, "linear", function(){
                    $("#project-web-tools-animate_2").css("position", "initial");
                });
            }
        });
    </script>
@endsection