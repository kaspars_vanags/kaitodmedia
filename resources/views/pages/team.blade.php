<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 20.06.2016.
 * Time: 11:07
 */

?>

@extends('welcome')

@section('body')
    <div class="row">
        <div id="team-title" class="col-md-8 col-md-offset-2">
            <h1 class="text-center">PROJECT TEAM</h1>
        </div>
    </div>
    <div class="row">
        <div id="team-body" class="col-md-8 col-md-push-2">
            <div class="row" style="margin-top: 30px; min-height: 600px;">
                    <div class="col-md-2 col-md-offset-5" style="background-image: url('img/team/test1.jpg'); background-size: 100%; border-radius: 50%; height: 200px; max-width: 100%;"></div>
{{--                <div class="col-md-4 col-md-push-4">
                    <img class="center-block" src="img/team/kdh.jpg" height="300">
                    <h2 class="text-center">KAITO D. HAWK</h2>
                </div>--}}
            </div>
        </div>
    </div>

@endsection
