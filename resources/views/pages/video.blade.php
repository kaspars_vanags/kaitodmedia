<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 20.06.2016.
 * Time: 22:28
 */
 ?>

@extends('welcome')

@section('body')
    <div class="row">
        <div id="video-title" class="col-md-8 col-md-offset-2">
            <h1 class="text-center">VIDEO</h1>
        </div>
    </div>
    <div class="row">
        <div id="video-body" class="col-md-8 col-md-push-2">
            <div class="row" style="margin-top: 30px; margin-bottom: 20px;">
                <div class="col-md-6 col-md-push-3">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/Lp7E973zozc" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection
