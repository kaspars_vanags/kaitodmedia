<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 19.06.2016.
 * Time: 19:22
 */
?>

@extends('welcome')

@section('body')
    <div class="row">
        <div id="about-us-title" class="col-md-8 col-md-offset-2">
           <h1 class="text-center">ABOUT K.D.H</h1>
        </div>
    </div>
    <div class="row">
        <div id="about-us-body" class="col-md-8 col-md-offset-2">
            <p class="text-center">
                Kas ir K.D.H? Tā ir doma, tā ir jauna domāšana. Brīdī, kad cilvēks atmet konceptu ķermenis,
                brīdi, kad Jūs saprotat, ka fiziskā massa ir tikai objekts, kas ļauj pārvietoties neticamam spēkam, kas var pārvērsties par ko vien grib. Tajā brīdī Jūs kļūstat par projekta domas nesēju.
            </p>
            <p class="text-center">
                Kas rada krāsas? Tas cik objekts absorbē un atgriež gaismu. Kas atšķir sētnieku no slavenības? Tas kādu iekšējo enerģiju tas atgriež. Mūsu ķermenis ir tikai čaula, bet mūsu enerģija ir kaut kas, ko mēs varam kontrolēt.
                Ja mēs vēlamies, mēs izstarosim dzelteno krāsu, bet ja gribēsim melno. Tāpat, ja gribēsim izstarosim neko, bet ja gribēsim izstarosim spēku un slavu.
            </p>
        </div>
    </div>
    <div class="row">
        <div id="about-us-body" class="col-md-8 col-md-offset-2">
            <p class="text-center">
                What is K.D.H? It's idea, idea for new thinking. When we obliterate concepts of body,
                when You understand that our body is only a object, which can move around amazing amount of power, which can transform into anything.
                That moment You become medium of idea.
            </p>
            <p class="text-center">
                 What makes colors? That how much light's absorbs object and return. What makes difference between janitor and superstar? What inner energy person returns.
                Our body's are only shell, but our energy is something we can control. If we want we return yellow color, but if we want we will return black.
                The same we can return nothing, but if we want we can return energy of power and fame.
            </p>
        </div>
    </div>

@endsection
