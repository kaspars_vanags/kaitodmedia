<!DOCTYPE html>
<html>
    <head>
        <!-- Initialize bootstrap -->
        <meta charset="utf-8">
        <meta name="_token" content="{!! csrf_token() !!}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>K.D.H | Media</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <!-- Material Design fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,700" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">


        <!-- Material Desgin CSS -->
        <link href="{{URL::asset('css/bootstrap.css')}}" rel="stylesheet">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

        <link href="{{URL::asset('css/app.css')}}" rel="stylesheet">

        <!-- Average Bootstrap -->
        <script src="https://www.atlasestateagents.co.uk/javascript/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="{{URL::asset('js/jquery-2.1.4.js')}}"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <style>

            .footer {
                <?php
                    $page = $_SERVER['REQUEST_URI'];

                    if($page == '/') {
                        //echo "position: absolute;";
                    } else {
                       // echo "margin-top: 30px;";
                    }
                ?>
            }

        </style>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-44508041-3', 'auto');
            ga('send', 'pageview');

        </script>
    </head>
    <body>
        @include("widgets.navbar")
        <div class="container-fluid">
            <div class="row">
                @yield('body')
            </div>
            @include("widgets.footer")
            @yield('scripts')
            <script>
                $(document).ready(function(){
                    $('#navbar-main').affix({offset: {top: 150} });
                    $('#navbar-main').on('affixed-top.bs.affix', function(){
                        $("#navbar-main").addClass("return-properties");
                    });

                    $('#navbar-main').on('affixed.bs.affix', function(){
                        $("#navbar-main").removeClass("return-properties");
                    });


                    $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

                        if (this.hash !== "") {
                            event.preventDefault();

                            var hash = this.hash;

                            $('html, body').animate({
                                scrollTop: $(hash).offset().top
                            }, 900, function(){

                                window.location.hash = hash;
                            });
                        }
                    });

                    $(window).scroll(function() {
                        $(".slideanim").each(function(){
                            var pos = $(this).offset().top;

                            var winTop = $(window).scrollTop();
                            if (pos < winTop + 600) {
                                $(this).addClass("slide");
                            }
                        });
                    });
                });

/*                $('#navbar-main').on('affixed-top.bs.affix', function(){
                    alert(1);
                    $("#navbar-main").addClass("return-properties");
                });*/
            </script>
           {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sketch.js/1.0/sketch.min.js"></script>--}}

         {{--   <script src="https://cdnjs.cloudflare.com/ajax/libs/dat-gui/0.6.2/dat.gui.min.js"></script>--}}

        </div>
    </body>
</html>
