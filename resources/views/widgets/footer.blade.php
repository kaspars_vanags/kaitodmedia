<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 09.03.2017.
 * Time: 19:43
 */

        ?>


<div class="row">
    <footer class="footer">
        <div class="container">
            <div class="row" style="margin-top: 100px;">
                <div class="col-md-5">
                    <h4 class="text-left"><span class="title" style="font-size: 1.5em;">KAITO.D.H</span><span class="title-second" style="font-size: 1.5em"> | MEDIA</span></h4>
                    <p class="text-justify" style="font-family: 'Lato', 'Arial'; font-weight: 600; color: #212121;">
                        We begin with improving local media environment. After completing first steps we keep going to
                        international market and with that we expand our business model.
                    </p>
                </div>
                <div class="col-md-7">
                    <ul class="footer-nav nav navbar-nav navbar-right">
                        <li><a href="/">KDH</a></li>
                        <li><a href="/#about-section">ABOUT</a></li>
                        <li><a href="/#example-section">WORK</a></li>
                        <li><a href="/#team-section">TEAM</a></li>
                        <li><a href="/#contact-section">CONTACT</a></li>
                    </ul>
                </div>
            </div>
            <div class="row" style="margin-top: 90px;">
                <div class="col-md-12">
                    <hr>
                    <div class="row">
                        <div class="col-md-1">
                            <a href="https://www.facebook.com/KaitoDH-Media-101296057072260/" target="_blank">
                                <div class="soc-hexagon">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                    <div class="soc-hexTop"></div>
                                    <div class="soc-hexBottom"></div>
                                </div>
                            </a>
                        </div>
{{--                        <div class="col-md-1">
                            <div class="soc-hexagon">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                <div class="soc-hexTop"></div>
                                <div class="soc-hexBottom"></div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="soc-hexagon">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                                <div class="soc-hexTop"></div>
                                <div class="soc-hexBottom"></div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="soc-hexagon">
                                <i class="fa fa-youtube" aria-hidden="true"></i>
                                <div class="soc-hexTop"></div>
                                <div class="soc-hexBottom"></div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="soc-hexagon">
                                <i class="fa fa-ra" aria-hidden="true"></i>
                                <div class="soc-hexTop"></div>
                                <div class="soc-hexBottom"></div>
                            </div>
                        </div>--}}
                        <div class="col-md-4" style="line-height: 40px;">
                            <p class="text-right" style="font-weight: 600">
                                <span class="title-second" style="font-size: 1em; font-weight: 600;">© 2017 &nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <span class="title-second" style="font-size: 1em; font-weight: 600;">KAITO.D.H | MEDIA</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
