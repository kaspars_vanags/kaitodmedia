<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 5/7/2018
 * Time: 14:28
 */

?>

<nav class="navbar navbar-fixed-top affix visible-xs" id="navbar-mobile">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collaps-kdh" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <h1><span class="title">KAITO.D.H</span><span class="title-second"> | MEDIA</span></h1>
        </div>
        <div class="collapse navbar-collapse" id="collaps-kdh">
            <ul class="nav navbar-nav navbar-right">
                <?php

                if($page == '/') {
                    echo '<li class="active"><a href="/">KDH <span class="sr-only">(current)</span></a></li>';
                } else {
                    echo '<li><a href="/">KDH</a></li>';
                }

                if($page == '/portfolio') {
                    echo '<li><a href="/#about-section">ABOUT <span class="sr-only">(current)</span></a></li>';
                } else {
                    echo '<li><a href="/#about-section">ABOUT</a></li>';
                }

                if($page == '/portfolio') {
                    echo '<li class="active"><a href="/portfolio">WORK <span class="sr-only">(current)</span></a></li>';
                } else {
                    echo '<li><a href="#example-section">WORK</a></li>';
                }

                if($page == '/portfolio') {
                    echo '<li><a href="/#team-section">TEAM <span class="sr-only">(current)</span></a></li>';
                } else {
                    echo '<li><a href="#team-section">TEAM</a></li>';
                }

                if($page == '/portfolio') {
                    echo '<li><a href="/#contact-section">CONTACT <span class="sr-only">(current)</span></a></li>';
                } else {
                    echo '<li><a href="#contact-section">CONTACT</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>

<nav class="navbar navbar-fixed-top hidden-xs" id="navbar-main" {{--data-spy="affix" data-offset-top="560" data-offset-bottom="100"--}}>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collaps-kdh" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <h1><span class="title">KAITO.D.H</span><span class="title-second"> | MEDIA</span></h1>
        </div>
        <div class="collapse navbar-collapse" id="collaps-kdh">
            <ul class="nav navbar-nav navbar-right">
                <?php

                if($page == '/') {
                    echo '<li class="active"><a href="/">KDH <span class="sr-only">(current)</span></a></li>';
                } else {
                    echo '<li><a href="/">KDH</a></li>';
                }

                if($page == '/portfolio') {
                    echo '<li><a href="/#about-section">ABOUT <span class="sr-only">(current)</span></a></li>';
                } else {
                    echo '<li><a href="/#about-section">ABOUT</a></li>';
                }

                if($page == '/portfolio') {
                    echo '<li class="active"><a href="/portfolio">WORK <span class="sr-only">(current)</span></a></li>';
                } else {
                    echo '<li><a href="#example-section">WORK</a></li>';
                }

                if($page == '/portfolio') {
                    echo '<li><a href="/#team-section">TEAM <span class="sr-only">(current)</span></a></li>';
                } else {
                    echo '<li><a href="#team-section">TEAM</a></li>';
                }

                if($page == '/portfolio') {
                    echo '<li><a href="/#contact-section">CONTACT <span class="sr-only">(current)</span></a></li>';
                } else {
                    echo '<li><a href="#contact-section">CONTACT</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>



