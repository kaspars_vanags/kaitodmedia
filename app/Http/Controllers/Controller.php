<?php

namespace App\Http\Controllers;

use App\Inbox;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function __construct(){

        $this->notfound();
    }

    public function notfound(){

        return view('errors.404');
    }

    public function index(){
        return view('pages.index');
    }

    public function about()
    {
        return view('pages.about');
    }


    public function team()
    {
        return view('pages.team');
    }

    public function portfolio()
    {
        return view('pages.project');
    }

    public function video()
    {
        return view('pages.video');
    }

    public function sendMail(Request $request) {

        $all = $request->all();

        try {

            if(strlen($all["body"]) > 400) {
                throw new \Exception("You message is too long.");
            }

            Inbox::create($all);
            flash("Your message has been delivered", "success");

            return redirect('/#contact-section');

        } catch (\Exception $e) {

            flash("Something went wrong", "danger");
            return redirect('/#contact-section');
        }

    }
}
