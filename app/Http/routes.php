<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('', "Controller@notfound");

Route::get('/', "Controller@index");
Route::post('/mail/send', "Controller@sendMail");
/*Route::get('/about', "Controller@about");
Route::get('/kdh', "Controller@team");*/
Route::get('/portfolio', "Controller@portfolio");
/*Route::get('/video', "Controller@video");*/
